# feed-orangepi-plus-support

## Description

This is an [OpenWrt feed](http://openwrt.org/docs/guide-developer/feeds) holding packages (all or mainly kernel modules) to support hardware functionality on [Xunlog OrangePi Plus](http://linux-sunxi.org/Xunlong_Orange_Pi_Plus)/ [Xunlog OrangePi Plus 2](http://linux-sunxi.org/Xunlong_Orange_Pi_Plus_2) that is not supported by [upstream OpenWrt](http://openwrt.org).

* The following is supported via this repository:
* The following is beeing worked on:
  - GPU driver and framebuffer driver (`CONFIG_DRM_LIMA`/ "sun4i-drm"/ framebuffer "sun4i-drmdrmfb") which allows selecting the video mode,
  - onboard [WLAN "RTL8189ETV"](https://linux-sunxi.org/Wifi#Realtek),
  - onboard audio support.
* The following is broken:
  - Infrared: In upstream OpenWrt, there is a [package `kmod-sunxi-ir`](https://openwrt.org/packages/pkgdata/kmod-sunxi-ir) which should support the onboard OpenWrt receiver, but it generates an empty package instead of containing the kernel module `sunxi-cir.ko`, see [this issue](https://github.com/openwrt/openwrt/issues/10466).
* The following is not (yet) supported:
  - Onboard camera interfacve.
* The following has not been tried out yet:
  - Onboard serial,
  - using the GPIO pins for different tasks.

## Usage

To use this feeds, add to your `feeds.conf`  
```
src-git  orangepiplussupport  https://gitlab.com/dreieckli-openwrt/feed-orangepi-plus-support.git
```  
and run `./scripts/feeds update -a && /scripts/feeds install -a`.

Then the packages defined here should appear in your OpenWrt configuration (`make menuconfig`). Run `make oldconfig` instead to directly be prompted about new configuration items.
