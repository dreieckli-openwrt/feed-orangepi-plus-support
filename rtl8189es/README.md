# OpenWrt feed for supporting Realtek RTL8189* network cards via external drivers by "jwrdegoede".

## Description

This is an OpenWrt package containing a driver (kernel module) for Realtek RTL8189* WiFi cards. The driver is maintained in an external repository at https://github.com/jwrdegoede/rtl8189ES_linux, see https://linux-sunxi.org/Wifi#RTL8189ES_.2F_RTL8189ETV.
